<?php

    require('func/functions.php');
    
    $naam = strtolower($_GET['name']);
    $naam = isset($_GET['name']) ? $_GET['name'] : "maxime";
    
    $currentLeerling = getLeerling($naam);
    
    $files = scandir("files"); 
    
    foreach ($files as $file) {
        if ($file != '.' && $file != '..') {
            $fileParts = explode('.',$file);
            $leerlingen[] = $fileParts[0];
        }
    }

?>
<html lang="nl">
<head>
    <meta charset="UTF-8"/>
    <title>6info</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="jumbotron jumbotron-fluid center" id="header">
                <div class="container center">
                    <h1 class="display-4">ZESINFO</h1>
                    <p class="lead">Dit is de eerste php applicatie voor de leerlingen van 6info</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h3 class="display-3">Leerlingen</h3>
                </div>
            </div>
            <!-- hier komt het overzicht van de leerlingen-->
            <div class="row">
                <div class="col-xs-12">
                    <div class="list-group">
                        <?php foreach ($leerlingen as $leerling) : ?>
                            <div class="list-group-item list-group-item-action ">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <a href="index.php?name=<?= $leerling; ?>"><?= $leerling; ?></a>
                                    </div>
                                    <div class="col-xs-2">
                                        <a href="edit.php?name=<?= $leerling; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                    </div>
                                    <div class="col-xs-2 text-right">
                                        <a href="delete.php?name=<?= $leerling; ?>"><span class="glyphicon glyphicon-trash"></span></a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
		                <a href="new.php" class="list-group-item list-group-item-action">Voeg leerling toe</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-8">
            <!-- hier komt het detail van de geselecteerde leerling -->
            <div class="row">
                <div class="col-xs-6">
                    <h1>Info</h1>
                    <p>Voornaam: <?= $currentLeerling['voornaam'] ?></p>
                    <p>Achternaam: <?= $currentLeerling['naam'] ?></p>
                    <p>Geboorteplaats: <?=  $currentLeerling['geboorte_plaats'] ?></p>
                    <p>Geboortedatum: <?=  $currentLeerling['geboorte_datum'] ?></p>
                    <ul>
                        <?php 
                        $activiteiten = explode(',', $currentLeerling['activiteiten']);
                        foreach($activiteiten as $activiteit) : ?>
                        <li>
                            <?= $activiteit; ?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-xs-6">
                    <img src="<?= $info[5]; ?>" style="width:100%"></img>
                </div>
            </div>
        </div>
    </div>
</div>

</body>