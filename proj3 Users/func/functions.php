<?php
        
    function getLeerling($leerling){
        
        $path = "files/".strtolower($leerling).".txt";
    
        $fileContent = file_get_contents($path); 
        
        $lines = explode(PHP_EOL, $fileContent);
    
        foreach ($lines as $line) {
            $line = trim($line);    
            $info[] = $line;
        }
    
        $voornaam = $info[0];
        $naam = $info[1];
        $date = explode('/',$info[2]);
        $geboorteDatum = $date[0]."-".$date[1]."-".$date[2];
        $geboortePlaats = $info[3];
        $activiteiten = $info[4];
        $afbeelding = $info[5];
        
        $currentLeerling = array(
            'voornaam' => $voornaam,
            'naam' => $naam,
            'geboorte_datum' => $geboorteDatum,
            'geboorte_plaats' => $geboortePlaats,
            'activiteiten' => $activiteiten,
            'afbeelding' => $afbeelding
        );
        
        return $currentLeerling;
    }
    
    function editLeerling($leerling){
    
        $geboorteDatum = new DateTime($info[2]);
    
        if(isset($_POST['submit'])){
            
            $path = "files/".strtolower($leerling).".txt";
            
            $voornaam = $_POST['voornaam'];
            $naam = $_POST['naam'];
            $geboorteDatum = $_POST['geboorte_datum'];
            $geboortePlaats = $_POST['geboorte_plaats'];
            $activiteiten = $_POST['activiteiten'];
            $afbeelding = $_POST['afbeelding'];
        
            unlink($path);
        
            $content = $voornaam."\r\n".$naam."\r\n".$geboorteDatum."\r\n".$geboortePlaats."\r\n".$activiteiten."\r\n".$afbeelding;
        
            file_put_contents("files/".strtolower($voornaam).".txt",$content);
        
            header('Location: edit.php?name='.$voornaam);
        }
        
        
    }
    
    function newLeerling(){
        
        if(isset($_POST['submit'])){

            $voornaam = $_POST['voornaam'];
            $naam = $_POST['naam'];
            $geboorteDatum = $_POST['geboorte_datum'];
            $geboortePlaats = $_POST['geboorte_plaats'];
            $activiteiten = $_POST['activiteiten'];
            $afbeelding = $_POST['afbeelding'];
        
            $content = $voornaam."\r\n".$naam."\r\n".$geboorteDatum."\r\n".$geboortePlaats."\r\n".$activiteiten."\r\n".$afbeelding;
        
            file_put_contents("files/".strtolower($voornaam).".txt",$content);
        
            header('Location: index.php?name='.strtolower($voornaam));
        }
        
    }
        
?>