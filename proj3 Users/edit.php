<?php

    require('func/functions.php');
    
    $naam = strtolower($_GET['name']);
    $naam = isset($_GET['name']) ? $_GET['name'] : "maxime";
    
    $currentLeerling = getLeerling($naam);
    
    editLeerling($naam);
    

?>

<html lang="nl">
<head>
    <meta charset="UTF-8"/>
    <title>6info - nieuwe leerling</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="jumbotron jumbotron-fluid center" id="header">
                <div class="container center">
                    <h1 class="display-4">ZESINFO</h1>
                    <p class="lead">Edit leerling</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-offset-1 col-xs-6">
            <form method="post">
                <div class="row form-group form-row">
                    <div class="col-xs-6 text-right">
                        <label for="voornaam">Voornaam</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="voornaam" name="voornaam" value="<?= $currentLeerling['voornaam']; ?>" placeholder="geef voornaam" />
                    </div>
                </div>
                <div class="row form-group form-row">
                    <div class="col-xs-6 text-right">
                        <label for="naam">Naam</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="naam" name="naam" value="<?= $currentLeerling['naam']; ?>" placeholder="geef naam" />
                    </div>
                </div>
                <div class="row form-group form-row">
                    <div class="col-xs-6 text-right">
                        <label for="datum">Geboortedatum</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="datum" name="geboorte_datum" value="<?= $currentLeerling['geboorte_datum']; ?>" placeholder="dd-mm-yyyy" />
                    </div>
                </div>
                <div class="row form-group form-row">
                    <div class="col-xs-6 text-right">
                        <label for="plaats">Geboorteplaats</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="plaats" name="geboorte_plaats" value="<?= $currentLeerling['geboorte_plaats']; ?>" placeholder="geef geboorteplaats" />
                    </div>
                </div>
                <div class="row form-group form-row">
                    <div class="col-xs-6 text-right">
                        <label for="activiteiten">Activiteiten</label>
                    </div>
                    <div class="col-xs-6">
                        <textarea class="form-control" id="activiteiten" name="activiteiten" rows="5"><?= $currentLeerling['activiteiten']; ?></textarea>
                    </div>
                </div>
                <div class="row form-group form-row">
                    <div class="col-xs-6 text-right">
                        <label for="afbeelding">Afbeelding</label>
                    </div>
                    <div class="col-xs-6">
                        <input type="text" class="form-control" id="afbeelding" name="afbeelding" value="<?= $currentLeerling['afbeelding']; ?>" placeholder="geef url van afbeelding" onchange="preview(this.value)" />
                    </div>
                </div>
                <div class="row form-group form-row">
                    <div class="col-xs-6 text-right">
                        <a class="button" href="index.php">Annuleer</a>
                    </div>
                    <div class="col-xs-6">
                        <input type="submit" name="submit" value="Bewaar" />
                    </div>
                </div>
            </form>
        </div>
        <div class="col-xs-5">
            <img id="preview" src="<?= $info[5]; ?>" style="width:100%" />
        </div>
    </div>
</div>

<script>
    function preview(url) {
        console.log(url);
        document.getElementById('preview').src = url;
    }
</script>
</body>

