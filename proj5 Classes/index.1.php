<?php

    class User{
        
        private $firstname = "";
        private $birthDate = null;
        
        public function __construct($firstname)
        {
            $this->firstname = $firstname; 
        }
        
        public function getFirstName()
        {
            
            return $this->firstname;
            
        }
        
        public function getBirthDate()
        {
            
            return $this->birthDate->format('Y-m-d');
            
        }
        
        public function setFirstName($firstname)
        {
            
            $this->firstname = $firstname;    
            return $this;
            
        }
        
        public function setBirthDate($birthDate)
        {
            
            $this->birthDate = new DateTime($birthDate);    
            return $this;
            
        }
    }

    
    $emile = new User("Emile");
    $emile->setFirstName("Emile")->setBirthDate("20-11-2001");
    var_dump($emile);
    
    $noah = new User();
    $noah->setFirstName("Noah");
    var_dump($noah);
    
    echo $emile->getFirstName() . " verjaart op " . $emile->getBirthDate();
?>