<?php

    class User{
        
        private $firstname = "";
        private $password = "";
        private $role = "";
        
        public function getFirstname()
        {
            
            return $this->firstname;
            
        }
        
        public function setFirstname($firstname)
        {
            
            $this->firstname = $firstname;    
            return $this;
            
        }
        
        public function getPassword()
        {
            
            return $this->password;
            
        }
        
        public function setPassword($password)
        {
            
            $this->password = $password;
            
        }
        
        public function getRole()
        {
            
            return $this->role;
            
        }
        
        public function setRole($role)
        {
            
            $this->role = $role;
            
        }
    }
    
    class UserRepository{
        
        const USERNAME_COLUMN = 0;
        const PASSWORD_COLUMN = 1;
        const ROLE_COLUMN = 2;
        const DELIMITOR = ";";
        
        private $path = "files/users.txt";
        
        public function find($username)
        {
            
            $content = file_get_contents($this->path);
            $lines = explode(PHP_EOL, $content);
            
            foreach ($lines as $line) {
               
                $userDetail = explode(self::DELIMITOR, $line);
                
                if($userDetail[self::USERNAME_COLUMN] == $username){
                    
                    $user = new User();
                    $user->setFirstname($userDetail[self::USERNAME_COLUMN]);
                    $user->setPassword($userDetail[self::PASSWORD_COLUMN]);
                    $user->setRole($userDetail[self::ROLE_COLUMN]);
                    
                    return $user;
                }
               
            }
            
            return null;
            
        }
        
    }
    
    class Authenticator{
    
        public function passwordHash($password){
            
            return md5(hash("SHA512",$password));
            
        }
        
        public function authenticate($user, $password){
            return $user->getPassword() == $password;
        }
        
    }
    
    $userRepository = new UserRepository();
    $maksym = $userRepository->find("maksym");
     
    $authenticator = new Authenticator();
    
    echo "Hashed password: " . $authenticator->passwordHash($maksym->getPassword);

    if($authenticator->authenticate($maksym,$password)) echo "welkom";
    else "sorry";

?>