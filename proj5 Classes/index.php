<?php

    class User{
        
        private $firstname = "";
        private $birthDate = null;
        
        public function __construct()
        {
            echo "Help ik word aangemaakt";   
        }
        
        public function getFirstName()
        {
            
            return $this->firstname;
            
        }
        
        public function getBirthDate()
        {
            
            return $this->birthDate->format('Y-m-d');
            
        }
        
        public function setFirstName($firstname)
        {
            
            $this->firstname = $firstname;    
            
        }
        
        public function setBirthDate($birthDate)
        {
            
            $this->birthDate = new DateTime($birthDate);    
            
        }
    }

    
    $emile = new User();
    $emile->setFirstName("Emile");
    $emile->setBirthDate("20-11-2001");
    var_dump($emile);
    
    $noah = new User();
    $noah->setFirstName("Noah");
    var_dump($noah);
    
    echo $emile->getFirstName() . " verjaart op " . $emile->getBirthDate();
?>