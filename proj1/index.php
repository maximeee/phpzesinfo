<?php
    $lijn1 = "Maxime";
    $lijn2 = "Vermeeren";
    $lijn3 = "08/08/2001";
    $lijn4 = "Oudenaarde";
    $lijn5 = "Budapest,Frankrijk,Malta,Feesten,Muziek";
    $lijn6 = "https://i.pinimg.com/originals/c6/b6/fe/c6b6fe66bef646decc4f4b69018c2970.png";
    
    $voorNaam = $lijn1;
    $achterNaam = $lijn2;
    $geboorteDatum = new DateTime($lijn3);
    $geboortePlaats = $lijn4;
    $activiteiten = explode(',', $lijn5);
    $afbeeldingComic = $lijn6;
?>
<html lang="nl">
<head>
    <meta charset="UTF-8"/>
    <title>6info</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="jumbotron jumbotron-fluid center" id="header">
                <div class="container center">
                    <h1 class="display-4">ZESINFO</h1>
                    <p class="lead">Dit is de eerste php applicatie voor de leerlingen van 6info</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h3 class="display-3">Leerlingen</h3>
                </div>
            </div>
            <!-- hier komt het overzicht van de leerlingen-->
        </div>
        <div class="col-xs-9">
            <!-- hier komt het detail van de geselecteerde leerling -->
            <h1>Info</h1>
            <p><?= $voorNaam; ?></p>
            <p><?= $achterNaam; ?></p>
            <p><?= $geboorteDatum->format('Y-m-d'); ?></p>
            <p><?= $geboortePlaats; ?></p>
            <ul>
                <?php foreach($activiteiten as $activiteit) : ?>
                    <li>
                        <?= $activiteit; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
            <img src="<?= $afbeeldingComic; ?>">
            
        </div>
    </div>
</div>

</body>

