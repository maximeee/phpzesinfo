<?php

    $naam = $_GET['name'];
    
    if(empty($name)){ $naam = 'maxime'; }
    
    $path = "leerlingen/".$naam.".txt";
    
    $fileContent = file_get_contents($path);     
    $lines = explode(PHP_EOL, $fileContent);
    
    foreach ($lines as $line) {
        $line = trim($line);                               
        $info[] = $line;
    }
    
    $activiteiten = explode(',', $info[4]);
    $geboorteDatum = new DateTime($info[2]);
    
    $files = scandir("leerlingen"); 
    
    foreach ($files as $file) {
        if ($file != '.' && $file != '..') {
            $fileParts = explode('.',$file);
            $leerlingen[] = $fileParts[0];
        }
    }


?>
<html lang="nl">
<head>
    <meta charset="UTF-8"/>
    <title>6info</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="jumbotron jumbotron-fluid center" id="header">
                <div class="container center">
                    <h1 class="display-4">ZESINFO</h1>
                    <p class="lead">Dit is de eerste php applicatie voor de leerlingen van 6info</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h3 class="display-3">Leerlingen</h3>
                    <?php foreach ($leerlingen as $leerling) : ?>
                        <p><button onclick="location.href='?name=<?= $leerling; ?>'"><?= $leerling; ?></button></p>
                    <?php endforeach; ?>
                </div>
            </div>
            <!-- hier komt het overzicht van de leerlingen-->
        </div>
        <div class="col-xs-9">
            <!-- hier komt het detail van de geselecteerde leerling -->
            <h1>Info</h1>
            <p>Voornaam: <?= $info[0]; ?></p>
            <p>Achternaam: <?= $info[1]; ?></p>
            <p>Geboorteplaats: <?= $geboorteDatum->format('Y-m-d'); ?></p>
            <p>Geboortedatum: <?= $info[3]; ?></p>
            <ul>
                <?php foreach($activiteiten as $activiteit) : ?>
                    <li>
                        <?= $activiteit; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
            <img src="<?= $info[5]; ?>"></img>
        </div>
    </div>
</div>

</body>

