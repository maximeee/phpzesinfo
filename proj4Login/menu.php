    <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Login app</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="#">Registreren</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="login.php">Inloggen</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="page1.php">Page 1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="page2.php">Page 2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Page 3</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Page 4</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="logout.php">Uitloggen</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>