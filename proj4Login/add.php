<?php

    $rank = "10";
    include('functions/functions.php');
    
    if(isset($_POST['add_submit'])){
        
        $username = strtolower($_POST['add_username']);
        $password = $_POST['add_password'];
        $mail = $_POST['add_mail'];
        $rank = $_POST['add_rank'];
        
        add($username,$password,$mail,$rank);
    
    }

    
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>One Page Wonder - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/one-page-wonder.min.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <?php include('menu.php'); ?>

    <header class="masthead text-center text-white">
      <div class="masthead-content">
        <div class="container">
          <form method="post" style="width:40%;margin: 0 auto;text-align:left;">
              <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input style="border:none;font-size:12px" name="add_username" type="text" class="form-control" id="exampleInputUsername1" aria-describedby="emailHelp" placeholder="Username..">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input style="border:none;font-size:12px" name="add_password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password..">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Mail</label>
                <input style="border:none;font-size:12px" name="add_mail" type="mail" class="form-control" id="exampleInputPassword1" placeholder="Mail..">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Rank</label>
                <input style="border:none;font-size:12px" name="add_rank" type="number" class="form-control" id="exampleInputPassword1" placeholder="Rank..">
              </div>
              <button name="add_submit" type="submit" class="btn btn-primary btn-xl rounded-pill mt-5">Inloggen</button>
            </form>
        </div>
      </div>
      <div class="bg-circle-1 bg-circle"></div>
      <div class="bg-circle-2 bg-circle"></div>
      <div class="bg-circle-3 bg-circle"></div>
      <div class="bg-circle-4 bg-circle"></div>
    </header>

    <section>
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-6 order-lg-2">
            <div class="p-5">
              <img class="img-fluid rounded-circle" src="img/03.jpg" alt="">
            </div>
          </div>
          <div class="col-lg-6 order-lg-1">
            <div class="p-5">
              <h2 class="display-4">Let there be rock!</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod aliquid, mollitia odio veniam sit iste esse assumenda amet aperiam exercitationem, ea animi blanditiis recusandae! Ratione voluptatum molestiae adipisci, beatae obcaecati.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer class="py-5 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white small">Copyright &copy; Your Website 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
